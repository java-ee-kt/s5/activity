package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	public void init() throws ServletException {
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		String firstName = req.getParameter("first_name");
		String lastName = req.getParameter("last_name");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discovery = req.getParameter("discovery");
		String dateOfBirth = req.getParameter("dob");
		String userType = req.getParameter("user_type");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession();
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discovery", discovery);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("userType", userType);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
		
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been initialized.");
	}
	
}
