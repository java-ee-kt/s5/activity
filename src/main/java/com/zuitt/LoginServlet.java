package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	
	public void init() throws ServletException {
		System.out.println("LoginServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		HttpSession session = req.getSession();
		String firstName = session.getAttribute("firstName").toString();
		String lastName = session.getAttribute("lastName").toString();
		
		String fullName = firstName + " " + lastName;
		
		session.setAttribute("fullName", fullName);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("LoginServlet has been initialized.");
	}
	
}
