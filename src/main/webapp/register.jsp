<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration Details</title>
</head>
<body>

	<h1>Registration confirmation</h1>
	<p>First Name: <%= session.getAttribute("firstName") %></p>
	<p>Last Name: <%= session.getAttribute("lastName") %></p>
	<p>Phone: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= session.getAttribute("discovery") %></p>
	<p>Date of Birth: <%= session.getAttribute("dateOfBirth") %></p>
	<p>User Type: <%= session.getAttribute("userType") %></p>
	<p>Description: <%= session.getAttribute("description") %></p>
	
	<form action="login" method="post">
		<input type="submit">
	</form>
	<!-- Link can be used but form button is used instead for visual purposes -->
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>

</body>
</html>