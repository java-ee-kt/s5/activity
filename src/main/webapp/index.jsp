<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Dynamic Content Activity</title>
<style>
	div {
		margin-top: 5px;
		margin-bottom: 5px;
	}
</style>
</head>
<body>

	<h1>Welcome to Servlet Job Finder!</h1>
	<form action="register" method="post">
		<div>
			<label for="first_name">First Name</label>
			<input type="text" name="first_name" required>
		</div>
		
		<div>
			<label for="last_name">Last Name</label>
			<input type="text" name="last_name" required>
		</div>

		<div>
			<label for="phone">Phone</label>
			<input type="text" name="phone">
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email">
		</div>
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			<input type="radio" id="friends" name="discovery" required value="friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="social_media" name="discovery" required value="social_media">
			<label for="social_media">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="discovery" required value="others">
			<label for="others">Others</label>
		</fieldset>
		
		
		<div>
			<label for="dob">Date of birth</label>
			<input type="date" name="dob" required>
		</div>
			
		<div>
			<label for="user_type">Are you an employer or applicant?</label>
			<select name="user_type">
				<option value="" selected="selected">Select One</option>
				<option value="employer">Employer</option>
				<option value="applicant">Applicant</option>
			</select>
		</div>
		
		<div>
			<label for="description">Profile Description</label>
			<textarea name="description" maxlength="500"></textarea>
		</div>
		
		<button>Register</button>
	</form>

</body>
</html>