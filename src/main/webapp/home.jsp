<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Servlet Job Finder Home</title>
</head>
<body>
	
	<%
		String userType = session.getAttribute("userType").toString();
	%>
	
	<h1>Welcome <%= session.getAttribute("fullName") %>!</h1>
	
	<%
		if(userType.equals("applicant")) {
			out.println("<p>Welcome applicant. You may now start looking for your career opportunity.</p>");
		} else if (userType.equals("employer")) {
			out.println("<p>Welcome employer. You may now start browsing applicant profiles.</p>");
		}
	%>
	
</body>
</html>